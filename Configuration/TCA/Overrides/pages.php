<?php
defined('TYPO3') || die();

$ll = 'LLL:EXT:backgroundimage/Resources/Private/Language/locallang.xlf:';

(function($table, $ll) {
    // Extra fields for the pages table
    $newPagesColumns = [
        'tx_backgroundimage_active' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_active',
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],
        'tx_backgroundimage_image' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_image',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'file',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_backgroundimage_image',
                ],
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'maxitems' => 1,
                'overrideChildTca' => [
                    'types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                ],
            ],
        ],
        'tx_backgroundimage_repeat' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_repeat',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        $ll . 'pages.tx_backgroundimage_default',
                        ''
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_repeat.repeat',
                        'repeat'
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_repeat.norepeat',
                        'no-repeat'
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_repeat.repeatx',
                        'repeat-x'
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_repeat.repeaty',
                        'repeat-y'
                    ],
                ],
            ],
        ],
        'tx_backgroundimage_color' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_color',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'input',
                'renderType' => 'colorpicker',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage_position' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_position',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'input',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage_size' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_size',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'input',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage_attachment' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => $ll . 'pages.tx_backgroundimage_attachment',
            'displayCond' => 'FIELD:tx_backgroundimage_active:REQ:true',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        $ll . 'pages.tx_backgroundimage_default',
                        ''
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_attachment.scroll',
                        'scroll'
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_attachment.fixed',
                        'fixed'
                    ],
                    [
                        $ll . 'pages.tx_backgroundimage_attachment.local',
                        'local'
                    ],
                ],
            ],
        ],
    ];

    // Adding fields to the pages table definition in TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($table, $newPagesColumns);

    // Create palette
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;LLL:EXT:backgroundimage/Resources/Private/Language/locallang.xlf:pages.palette.backgroundimage;backgroundimage', '', 'after:media');
    $GLOBALS['TCA'][$table]['palettes']['backgroundimage']['showitem'] = 'tx_backgroundimage_active, --linebreak--, tx_backgroundimage_image, --linebreak--, tx_backgroundimage_repeat, tx_backgroundimage_color, tx_backgroundimage_position, tx_backgroundimage_size, --linebreak--, tx_backgroundimage_attachment';

})('pages', $ll);
