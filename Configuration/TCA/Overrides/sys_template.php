<?php
defined('TYPO3') || die();

// Add static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('backgroundimage', 'Configuration/TypoScript/', 'Background image');
