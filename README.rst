=============
Documentation
=============

----------------
What does it do?
----------------

You can include a background image to your page. All you have to do is to upload the file in the page properties. This extension writes CSS code which references this image to the tag (e.g. "body"), id or class (e.g. "h2.layout-1") you configured in TypoScript constants.
If configured, the "slide" feature is used, that means an image is used for all subpages.


-----------
Screenshots
-----------

You can upload one image in page properties:

.. figure:: Documentation/screenshot1.png

    This extension doesn't flood your BE form with fields: The six fields are only visible if you check the checkbox "Activate background image".


-------------
Configuration
-------------

After installing this extension via the extension manager and updating the database, you have to include the extension template in field "Include static (from extensions)" in your main template.

Then upload your image file in the properties of a page, for example on the root page.


Overview of TypoScript constants
================================

==========  =========  =========  ===========
Property    Data type  Default    Description
==========  =========  =========  ===========
slide       boolean    0          Inherit picture from parent pages if you haven't uploaded one for the actual page.
target      string     body       Specify here, where your uploaded image should be shown, for example
                                  **#someId** for a specific id
                                  **.csc-textpic-text** for pagecontent type "Text with image"
                                  **body#page_324** h1 for all headings on a specific page
                                  If this parameter is left empty, the background image is set for **body**
==========  =========  =========  ===========


Example
=======

::

    plugin.backgroundimage {
        slide = 0
        target = div#myIdForDiv
    }



Example: Manipulate background image
====================================

You are able to manipulate the background image, e.g. make it gray:

::

    page.cssInline.440 {
        10.file.params = -colorspace GRAY
    }


----------
To-Do list
----------

Enable editors to set opacity of background image (CSS code generation needs to be done in PHP then).

Enable different background images and settings in alternative languages.

Sponsoring is welcome.

Write me if you have any suggestions.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
