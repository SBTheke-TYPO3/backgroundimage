<?php
defined('TYPO3') || die();

// Adding fields to rootline
$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= (empty($GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields']) === TRUE ? '' : ',') . 'tx_backgroundimage_active, tx_backgroundimage_image, tx_backgroundimage_repeat, tx_backgroundimage_color, tx_backgroundimage_position, tx_backgroundimage_size, tx_backgroundimage_attachment';