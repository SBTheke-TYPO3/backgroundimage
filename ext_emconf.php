<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "backgroundimage".
 *
 * Auto generated 23-12-2023 08:59
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Background Image',
  'description' => 'Enables uploading of an image in page properties, which then is shown on page as background image for a predefined element.',
  'category' => 'fe',
  'version' => '5.0.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '12.4.0-12.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

